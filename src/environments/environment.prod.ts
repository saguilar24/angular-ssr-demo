export const environment = {
  production: true,
  application:
  {
    name: 'angular-ssr-demo',
    angular: 'Angular 13.2.3',
    bootstrap: 'Bootstrap 5.1.3',
    fontawesome: 'Font Awesome 6.0.0',
  }
};
